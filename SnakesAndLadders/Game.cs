﻿using SnakesAndLadders.Interfaces;

namespace SnakesAndLadders
{
    public class Game :IGame
    {
        private readonly IConfig _config;
        private readonly IDie _die;
        private readonly IToken _token;

        public Game(IConfig config, IDie die, IToken token)
        {
            _config = config;
            _die = die;
            _token = token;
        }

        public void Start()
        {
            _token.SetTokenLocation(_config.StartPosition());
        }

        public void RollDieAndMove()
        {
            var dieValue = _die.RollDie();
            if (_token.GetTokenLocation() + dieValue <= _config.WinPosition())
            {
                _token.SetTokenLocation(_token.GetTokenLocation() + dieValue);
            }
        }

        public bool CheckForWin()
        {
            return _token.GetTokenLocation() == _config.WinPosition();
        }
    }
}