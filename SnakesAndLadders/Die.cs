﻿using System;
using SnakesAndLadders.Interfaces;

namespace SnakesAndLadders
{
    public class Die : IDie
    {
        private readonly IConfig _config;

        public Die(IConfig config)
        {
            _config = config;
        }

        public int RollDie()
        {
            return new Random().Next(1, _config.DieFaces() + 1);
        }
    }
}
