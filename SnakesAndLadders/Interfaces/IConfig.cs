﻿namespace SnakesAndLadders.Interfaces
{
    public interface IConfig
    {
        int StartPosition();
        int WinPosition();
        int DieFaces();
    }
}