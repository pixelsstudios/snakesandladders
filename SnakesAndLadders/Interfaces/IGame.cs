﻿namespace SnakesAndLadders.Interfaces
{
    public interface IGame
    {
        void Start();
        void RollDieAndMove();
        bool CheckForWin();
    }
}