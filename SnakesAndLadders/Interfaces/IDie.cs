﻿namespace SnakesAndLadders.Interfaces
{
    public interface IDie
    {
        int RollDie();
    }
}