﻿namespace SnakesAndLadders.Interfaces
{
    public interface IToken
    {
        int GetTokenLocation();
        void SetTokenLocation(int value);
    }
}