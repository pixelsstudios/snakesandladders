﻿using SnakesAndLadders.Interfaces;

namespace SnakesAndLadders
{
    public class Token : IToken
    {
        private int _tokenLocation;

        public int GetTokenLocation()
        {
            return _tokenLocation;
        }

        public void SetTokenLocation(int value)
        {
            _tokenLocation = value;
        }
    }
}