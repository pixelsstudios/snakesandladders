# SnakesAndLadders

I started the project by creating an Interfaces folder and populating it with all the components I thought I would need. I then added methods to the interfaces. As a game is essentially a set of rules around components and conditions, most of the game logic would be in the Game class. The Die has a Roll method to return a value, but a token does not have any properties or actions; its "value" comes from where it is in the game. I did not add a Player as this is not requested until later features.

I purposely left CheckForWin as a seperate call, rather than one automatically triggered by RollDieAndMove, because it's very likely a front-end will have seperate animations in that even. The same would be true of moving onto a snake or ladder to allow sounds to be played, etc.

I created tests to match the conditions detailed in the instructions for the Game class, then wrote the code to make them pass. I then did the same with the die class, although this could not be substituted and had to be tested by repetition. I then made the Token a class, as while it was not necessary for this Feature it would be as soon as there was more than 1. This was a forward-thinking implementation of what was asked for, rather than a layer that wasn't asked for like Players.

Some of the code would need restructuring as the features were added, e.g., a token would belong to a player. If this was a real-world development, I would have read ahead to know what was coming, and architect the code in a way that would require the least predictable re-work.