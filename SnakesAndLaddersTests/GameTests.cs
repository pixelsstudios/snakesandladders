using System.Diagnostics.CodeAnalysis;
using NSubstitute;
using NUnit.Framework;
using SnakesAndLadders;
using SnakesAndLadders.Interfaces;

namespace SnakesAndLaddersTests
{
    [ExcludeFromCodeCoverage]
    public class GameTests
    {
        [TestFixture]
        public class Tests
        {
            private IGame _game;
            private IConfig _config;
            private IDie _die;

            [SetUp]
            public void Setup()
            {
                _config = Substitute.For<IConfig>();
                _die = Substitute.For<IDie>();

                _config.DieFaces().Returns(6);
                _config.StartPosition().Returns(1);
                _config.WinPosition().Returns(100);
            }

            [Test]
            [TestCase(1, ExpectedResult = 1)]
            [TestCase(5, ExpectedResult = 5)]
            public int Given_AGameIsStarted_Then_TokenShouldBeAtStart(int startPosition)
            {
                // Arrange
                _config.StartPosition().Returns(startPosition);
                var token = new Token();
                _game = new Game(_config, _die, token);

                // Act
                _game.Start();

                // Assert
                return token.GetTokenLocation();
            }

            [Test]
            [TestCase(1, new[] {3}, ExpectedResult = 4)]
            [TestCase(1, new[] {3, 4}, ExpectedResult = 8)]
            public int Given_DieIsRolled_Then_TokenShouldMove(int tokenPosition, int[] dieValue)
            {
                // Arrange
                _config.StartPosition().Returns(tokenPosition);
                var token = new Token();
                _game = new Game(_config, _die, token);

                // Act
                _game.Start();
                foreach (var move in dieValue)
                {
                    _die.RollDie().Returns(move);
                    _game.RollDieAndMove();
                }

                // Assert
                return token.GetTokenLocation();
            }

            [Test]
            [TestCase(97, new[] {3}, ExpectedResult = true)]
            [TestCase(97, new[] {4}, ExpectedResult = false)]
            public bool Given_DieIsRolledWithinRangeOfWin_Then_ShouldWinIfLandsOnItExactly(int tokenPosition,
                int[] dieValue)
            {
                // Arrange
                _config.StartPosition().Returns(tokenPosition);
                var token = new Token();
                _game = new Game(_config, _die, token);

                // Act
                _game.Start();
                foreach (var move in dieValue)
                {
                    _die.RollDie().Returns(move);
                    _game.RollDieAndMove();
                }

                // Assert
                return _game.CheckForWin();
            }
        }
    }
}