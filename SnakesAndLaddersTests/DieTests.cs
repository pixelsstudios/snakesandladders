﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;
using NSubstitute;
using NUnit.Framework;
using SnakesAndLadders;
using SnakesAndLadders.Interfaces;

namespace SnakesAndLaddersTests
{
    [ExcludeFromCodeCoverage]
    public class DieTests
    {
        [TestFixture]
        public class Tests
        {
            private IGame _game;
            private IConfig _config;
            private IDie _die;
            private IToken _token;

            [SetUp]
            public void Setup()
            {
                _config = Substitute.For<IConfig>();
                _die = Substitute.For<IDie>();
                _token = Substitute.For<IToken>();

                _config.DieFaces().Returns(6);
                _config.StartPosition().Returns(1);
                _config.WinPosition().Returns(100);
            }

            [Test]
            [TestCase(6)]
            public void Given_ARandomNumberGenerator_Then_NumbersShouldBeInclusive(int dieFaces)
            {
                // Arrange
                _config.DieFaces().Returns(dieFaces);
                Die die = new Die(_config);
                _game = new Game(_config, die, _token);

                int[] results = new int[dieFaces];

                // Act
                for (int roll = 0; roll < dieFaces * dieFaces + dieFaces; roll++)
                {
                    var value = die.RollDie();
                    results[value - 1]++;
                }

                // Assert
                Assert.IsTrue(results.ToList().All(x => x > 0), "Some faces were never rolled");
            }

            [Test]
            [TestCase(6)]
            public void Given_ARandomNumberGenerator_Then_NumbersShouldBeWellDistributed(int dieFaces)
            {
                // Arrange
                _config.DieFaces().Returns(dieFaces);
                Die die = new Die(_config);
                _game = new Game(_config, die, _token);

                int[] results = new int[dieFaces];

                // Act
                for (int roll = 0; roll < dieFaces * dieFaces + dieFaces; roll++)
                {
                    var value = die.RollDie();
                    results[value - 1]++;
                }

                // Assert
                Assert.IsTrue(results.ToList().All(x => x > dieFaces * 0.2 && x < dieFaces * 2.2));
            }

            [Test]
            [TestCase(6)]
            public void Given_ARandomNumberGenerator_Then_NumbersShouldBeWithingDieRange(int dieFaces)
            {
                // Arrange
                _config.DieFaces().Returns(dieFaces);
                Die die = new Die(_config);
                _game = new Game(_config, die, _token);

                int errorCount = 0;

                // Act
                for (int roll = 0; roll < dieFaces * dieFaces + dieFaces; roll++)
                {
                    var value = die.RollDie();
                    if (value == 0 || value > dieFaces)
                    {
                        errorCount++;
                    }
                }

                // Assert
                Assert.AreEqual(0, errorCount);
            }
        }
    }
}
